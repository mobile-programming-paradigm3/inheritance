class PlayerFootballer {
  var name;
  var nationality;
  var age;

  PlayerFootballer(String name, String nationality, String age) {
    this.name = name;
    this.nationality = nationality;
    this.age = age;
  }

  void history() {
    print("ชื่อ : " + name + "\nสัญชาติ : " + nationality + "\nอายุ : " + age);
  }
  void club(){
    print("สโมสร");
  }
  void league(){
    print("ลีกที่เล่น");
  }
  void WagePerWeek(){
    print("ค่าเหนื่อยต่อสัปดาห์");
  }
}

class Specialty {
  void specialty() {
    print("ความโดดเด่น");
  }
}

mixin Position {
  void position() {
    print("ตำแหน่ง");
  }
}


abstract class GK extends PlayerFootballer with Position implements Specialty {
  GK(String name, String nationality, String age) : super(name, nationality, age);
  @override
  void specialty() {
    print("โดดเด่นเรื่อง : เซฟจุดโทษ , ขว้างบอลไกล");
  }

  @override
  void position() {
    print("ตำแหน่ง : ผู้รักษาประตู");
  }
}

abstract class DF extends PlayerFootballer with Position implements Specialty {
  DF(String name, String nationality, String age) : super(name, nationality, age);

  @override
  void specialty() {
    print("โดดเด่นเรื่อง : ความแข็งแกร่ง , ลูกกลางอากาศ");
  }

  @override
  void position() {
    print("ตำแหน่ง : กองหลัง");
  }
}

abstract class MF extends PlayerFootballer with Position implements Specialty {
  MF(String name, String nationality, String age) : super(name, nationality, age);
  @override
  void position() {
    print("ตำแหน่ง : กองกลาง");
  }

  @override
  void specialty() {
    print("โดดเด่นเรื่อง : ยิงไกล , จ่ายบอล , คล่องตัว");
  }

}

abstract class CF extends PlayerFootballer with Position implements Specialty {
  CF(String name, String nationality, String age) : super(name, nationality, age);
  @override
  void position() {
    print("ตำแหน่ง : กองหน้า");
  }

  @override
  void specialty() {
    print("โดดเด่นเรื่อง : ความเร็ว , จบสกอร์ , เลี้ยงบอล");
  }
}

class DaviddeGea extends GK{
  DaviddeGea(String name, String nationality, String age) : super(name, nationality, age);
  @override
  void club(){
    print("สโมสร : แมนเชสเตอร์ยูไนเต็ด");
  }
  @override
  void league(){
    print("ลีก : พรีเมียร์ลีกอังกฤษ");
  }
  @override
  void WagePerWeek(){
    print("ค่าเหนื่อย/สัปดาห์ : 375,000 ปอนด์");
  }

}
class SergioRamos extends DF{
  SergioRamos(String name, String nationality, String age) : super(name, nationality, age);
  @override
  void club(){
    print("สโมสร : ปารีแซ็ง-แฌร์แม็ง");
  }
  @override
  void league(){
    print("ลีก : ลีกเอิงฝรั่งเศส");
  }
  @override
  void WagePerWeek(){
    print("ค่าเหนื่อย/สัปดาห์ : 200,000 ปอนด์");
  }
  
}

class KevinDeBruyne extends MF{
  KevinDeBruyne(String name, String nationality, String age) : super(name, nationality, age);
  @override
  void club(){
    print("สโมสร : แมนเชสเตอร์ซิตี้");
  }
  @override
  void league(){
    print("ลีก : พรีเมียร์ลีกอังกฤษ");
  }
  @override
  void WagePerWeek(){
    print("ค่าเหนื่อย/สัปดาห์ : 385,000 ปอนด์");
  }
}

class ErlingHaaland extends CF{
  ErlingHaaland(String name, String nationality, String age) : super(name, nationality, age);
  @override
  void club(){
    print("สโมสร : แมนเชสเตอร์ซิตี้");
  }
  @override
  void league(){
    print("ลีก : พรีเมียร์ลีกอังกฤษ");
  }
  @override
  void WagePerWeek(){
    print("ค่าเหนื่อย/สัปดาห์ : 375,000 ปอนด์");
  }
  
}



void main(List<String> arguments) {
  DaviddeGea ddg = new DaviddeGea("DaviddeGea","สเปน", "31");
  DaviDeGea(ddg);
  print("________________________________________________");
  SergioRamos sr = new SergioRamos("SergioRamos", "สเปน", "36");
  Ramos(sr);
  print("________________________________________________");
  KevinDeBruyne kdb = new KevinDeBruyne("KevinDeBruyne", "เบลเยี่ยม", "31");
  KDB(kdb);
  print("________________________________________________");
  ErlingHaaland hl = new ErlingHaaland("ErlingHaaland", "นอร์เวย์", "22");
  Haaland(hl);

}

void Haaland(ErlingHaaland hl) {
  hl.history();
  hl.position();
  hl.specialty();
  hl.club();
  hl.league();
  hl.WagePerWeek();
}

void KDB(KevinDeBruyne kdb) {
  kdb.history();
  kdb.position();
  kdb.specialty();
  kdb.club();
  kdb.league();
  kdb.WagePerWeek();
}

void Ramos(SergioRamos sr) {
  sr.history();
  sr.position();
  sr.specialty();
  sr.club();
  sr.league();
  sr.WagePerWeek();
}

void DaviDeGea(DaviddeGea ddg) {
  ddg.history();
  ddg.position();
  ddg.specialty();
  ddg.club();
  ddg.league();
  ddg.WagePerWeek();
}
